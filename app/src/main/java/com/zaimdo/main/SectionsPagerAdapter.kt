package com.zaimdo.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.zaimdo.MainClass.Companion.first
import com.zaimdo.MainClass.Companion.numberOfTabs
import com.zaimdo.MainClass.Companion.second
import com.zaimdo.MainClass.Companion.third
import com.zaimdo.fragments.AllOffers
import com.zaimdo.fragments.BadList
import com.zaimdo.fragments.ZeroList
import com.zaimdo.uiactivities.Splash


private val TAB_TITLES = arrayOf(
    first,
    second,
    third
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return when (position){
            0 -> AllOffers()
            1 -> BadList()
            2 -> ZeroList()
            else -> AllOffers()
        }

    }

    override fun getPageTitle(position: Int): String? {
        return TAB_TITLES[position]
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return numberOfTabs
    }
}