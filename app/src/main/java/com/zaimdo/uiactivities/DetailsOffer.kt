package com.zaimdo.uiactivities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustEvent
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.zaimdo.MainClass.Companion.listDataAll
import com.zaimdo.R

class DetailsOffer : AppCompatActivity() {

    //progressbar declaring
    var progressBar: ProgressBar? = null

    //scroll view declaring
    var scrollView: ScrollView? = null

    //declaring toolbar
    var toolbar: Toolbar? = null

    //string position number
    var position: Int? = null

    //image of offer
    lateinit var imageView: ImageView

    //progressBarImage
    var progressBarImage: ProgressBar? = null

    //constraint
    var topAlert: ConstraintLayout? = null

    //text views
    lateinit var textAdress: TextView
    lateinit var textNumber: TextView
    lateinit var textMail: TextView
    lateinit var textSite: TextView
    lateinit var textPercent: TextView
    lateinit var textLicense: TextView
    lateinit var textTerms: TextView
    lateinit var textFistCredit: TextView
    lateinit var textNextCredit: TextView
    lateinit var textYUR: TextView
    lateinit var textName: TextView




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_offer)

        //event to track offer details pages opened
        val adjustEvent = AdjustEvent("ozp2zw")
        Adjust.trackEvent(adjustEvent)


        //declaring items
        declareItems()

        //getting intent
        val intent = intent

        //getting extra number from cloak adapter
        position = intent.getIntExtra("position", 0)

        //starting method
        parseDataToObjects()
        textTerms.paintFlags = textTerms.paintFlags or Paint.UNDERLINE_TEXT_FLAG




        textTerms.setOnClickListener {
            val intent =
                Intent(this@DetailsOffer, InfoCloak::class.java)
            intent.putExtra("check", 1)
            startActivity(intent)
        }

        val mToolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        mToolbar.setNavigationOnClickListener {
            startActivity(Intent(this@DetailsOffer, Cloak::class.java))
        }

    }

    //declaring items
    private fun declareItems() {
        //initializing views
        textTerms = findViewById(R.id.textTerms)
        //topAlert = findViewById(R.id.topConstraint)
        scrollView = findViewById(R.id.scrollView2)
        imageView = findViewById(R.id.imageOffer)
        textAdress = findViewById(R.id.textAdress)
        textNumber = findViewById(R.id.textNumber)
        textMail = findViewById(R.id.textMail)
        textSite = findViewById(R.id.textSite)
        textPercent = findViewById(R.id.textPercent)
        textLicense = findViewById(R.id.textLicense)
        textFistCredit = findViewById(R.id.textFirstCredit)
        textNextCredit = findViewById(R.id.textNextCredit)
        textYUR = findViewById(R.id.textYUR)
        textName = findViewById(R.id.textName)

        //initializing progressBars
        progressBar = findViewById(R.id.progressBar4)
       // progressBarImage = findViewById(R.id.progressbarImage)

    }

    // parse data to fields
    @SuppressLint("SetTextI18n")
    private fun parseDataToObjects() {
        //parsing data to views
        scrollView!!.visibility = View.VISIBLE
        progressBar!!.isIndeterminate = false
        progressBar!!.visibility = View.GONE
        val URL: String = listDataAll[position!!].img


        textAdress.text = "• " + listDataAll[position!!].offer_name
        textNumber.text = "• " + listDataAll[position!!].detail.phone
        textMail.text = "• " + listDataAll[position!!].detail.email
        textSite.text = "• " + listDataAll[position!!].detail.site
        textPercent.text = "• " + listDataAll[position!!].detail.apr
        textLicense.text = "• " + listDataAll[position!!].detail.license
        textFistCredit.text = "• " + listDataAll[position!!].amount.from + "₴"
        textNextCredit.text = "• " + listDataAll[position!!].amount.to + "₴"
        textYUR.text = "• " + listDataAll[position!!].detail.address
        textName.text = listDataAll[position!!].offer_name

        //parsing image to imageview
        Glide.with(this)
            .load(URL)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(p0: GlideException?, p1: Any?, p2: Target<Drawable>?, p3: Boolean): Boolean {
                    Log.d("GLIDE", "FAILED")
                  //  progressBarGlide.isIndeterminate = false
                  //  progressBarGlide.visibility = View.GONE
                    return false
                }
                override fun onResourceReady(p0: Drawable?, p1: Any?, p2: Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                  //  holder.progressBarGlide.isIndeterminate = false
                  //  holder.progressBarGlide.visibility = View.GONE
                    //do something when picture already loaded
                    return false
                }
            })
            .into(imageView)




    }

}