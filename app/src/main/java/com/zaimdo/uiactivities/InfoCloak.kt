package com.zaimdo.uiactivities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.zaimdo.R


class InfoCloak : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_details)

        val mToolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        mToolbar.setNavigationOnClickListener {
            startActivity(Intent(this@InfoCloak, Cloak::class.java))
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this@InfoCloak, Cloak::class.java))
    }
}