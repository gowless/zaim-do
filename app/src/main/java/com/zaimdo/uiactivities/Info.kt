package com.zaimdo.uiactivities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustEvent
import com.zaimdo.R


class Info : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val mToolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        mToolbar.setNavigationOnClickListener {
            startActivity(Intent(this@Info, MainActivity::class.java))
        }

        //event to track offer details pages opened
        val adjustEvent = AdjustEvent("uhkm6h")
        Adjust.trackEvent(adjustEvent)

    }

    override fun onBackPressed() {
        startActivity(Intent(this@Info, MainActivity::class.java))
    }

}