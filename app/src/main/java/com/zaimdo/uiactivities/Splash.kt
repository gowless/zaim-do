package com.zaimdo.uiactivities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustConfig
import com.adjust.sdk.LogLevel
import com.zaimdo.network.connect.HerokuEndpoints
import com.zaimdo.network.connect.ServiceBuilder
import com.facebook.applinks.AppLinkData
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.zaimdo.MainClass
import com.zaimdo.MainClass.Companion.ad_id
import com.zaimdo.MainClass.Companion.adgroup
import com.zaimdo.MainClass.Companion.adid
import com.zaimdo.MainClass.Companion.appToken
import com.zaimdo.MainClass.Companion.campaign
import com.zaimdo.MainClass.Companion.carrier
import com.zaimdo.MainClass.Companion.creative
import com.zaimdo.MainClass.Companion.editor
import com.zaimdo.MainClass.Companion.first
import com.zaimdo.MainClass.Companion.isEmpty
import com.zaimdo.MainClass.Companion.iteratedListBad
import com.zaimdo.MainClass.Companion.iteratedListZero
import com.zaimdo.MainClass.Companion.listDataAll
import com.zaimdo.MainClass.Companion.listDataBad
import com.zaimdo.MainClass.Companion.listDataZero
import com.zaimdo.MainClass.Companion.network
import com.zaimdo.MainClass.Companion.numberOfTabs
import com.zaimdo.MainClass.Companion.second
import com.zaimdo.MainClass.Companion.settings
import com.zaimdo.MainClass.Companion.third
import com.zaimdo.MainClass.Companion.trackerName
import com.zaimdo.MainClass.Companion.trackerToken
import com.zaimdo.R
import com.zaimdo.network.models.getmodels.Data
import com.zaimdo.network.models.getmodels.Listoffers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Splash : AppCompatActivity() {

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        //configure shared prefs
        settings = getSharedPreferences("LOCAL", Context.MODE_PRIVATE)
        editor = settings.edit()

        //get ad_id
        getID()

        //getting carrier ISO name
        getCarrier()

        //checking carrier to match current country code SIM
        if (carrier == "ua") {
            getJsonData()
        } else {
            getJsonDataCloak()
        }

    }

    //setting to get json file and parse it to models in main case
    fun getJsonData() {
        val request = ServiceBuilder.buildService(HerokuEndpoints::class.java)
        val call = request.getData(MainClass.REGION, MainClass.APPID)

        call.enqueue(object : Callback<Data> {
            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                if (response.isSuccessful) {

                    Log.d("TASG", response.body()?.categories?.get(1).toString())
                    listDataAll = response.body()!!.listoffers
                    isEmpty = false
                    numberOfTabs = response.body()!!.categories.size
                    listDataBad = response.body()!!.listoffers
                    listDataZero = response.body()!!.listoffers


                    //iterator for 2-nd tab (new list)
                    val x: Iterator<Listoffers> = listDataZero.listIterator()
                    while (x.hasNext()) {
                        val s: Listoffers = x.next()
                        if (s.categories.contains("zero")) {
                            iteratedListZero.add(s)
                        } else {
                            //x.remove();
                        }
                    }



                    //iterator for 3-rd tab (bad list)
                    val i: Iterator<Listoffers> = listDataBad.listIterator()
                    while (i.hasNext()) {
                        val m: Listoffers = i.next()
                        if (m.categories.contains("badCreditHistory")) {
                            iteratedListBad.add(m)
                        } else {
                            //  i.remove();
                        }
                    }


                    when (numberOfTabs) {
                        0 -> {
                        }
                        1 -> first = response.body()!!.categories[0].label

                        2 -> {
                            first = response.body()!!.categories[0].label
                            second = response.body()!!.categories[1].label
                        }
                        3 -> {
                            first = response.body()!!.categories[0].label
                            second = response.body()!!.categories[1].label
                            third = response.body()!!.categories[2].label
                        }
                    }

                    //adding tag of first launch
                    if (settings.getInt("firstlaunch", 0) == 1) {
                        //open MainActivity
                        getBeforeMain()
                    } else {
                        adjustDataGet()
                    }

                }
            }

            override fun onFailure(call: Call<Data>, t: Throwable) {
                Toast.makeText(this@Splash, "${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    //setting to get json file and parse it to models in main case
    fun getJsonDataCloak() {
        val request = ServiceBuilder.buildService(HerokuEndpoints::class.java)
        val call = request.getData(MainClass.REGION, MainClass.APPID)

        call.enqueue(object : Callback<Data> {
            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                if (response.isSuccessful) {

                    Log.d("TASG", response.body()?.categories?.get(1).toString())
                    listDataAll = response.body()!!.listoffers
                    isEmpty = false
                    numberOfTabs = response.body()!!.categories.size
                    listDataBad = response.body()!!.listoffers
                    listDataZero = response.body()!!.listoffers
                    when (numberOfTabs) {
                        0 -> {
                        }
                        1 -> first = response.body()!!.categories.get(0).label

                        2 -> {
                            first = response.body()!!.categories.get(0).label
                            second = response.body()!!.categories.get(1).label
                        }
                        3 -> {
                            first = response.body()!!.categories.get(0).label
                            second = response.body()!!.categories.get(1).label
                            third = response.body()!!.categories.get(2).label
                        }
                    }
                    //open MainActivity
                    getCloak()
                }
            }

            override fun onFailure(call: Call<Data>, t: Throwable) {
                Toast.makeText(this@Splash, "${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }


    //starting CloakActivity
    fun getCloak() {
        startActivity(Intent(this@Splash, Cloak::class.java))
    }

    //get carrier name
    fun getCarrier() {
        val manager =
                getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        carrier = manager.simCountryIso
    }

    //starting BeforeMain activity
    fun getBeforeMain() {
        startActivity(Intent(this@Splash, BeforeMain::class.java))
    }

    //getting advertising ID
    fun getID() {
        AsyncTask.execute {
            try {
                val adInfo =
                        AdvertisingIdClient.getAdvertisingIdInfo(this@Splash)
                ad_id = adInfo?.id.toString()
            } catch (e: Exception) {
            }
        }
    }

    //getting deep link data
    fun getDeepLink() {
        try {
            AppLinkData.fetchDeferredAppLinkData(applicationContext) { appLinkData ->
                if (appLinkData?.targetUri == null) {
                    editor.putString("sub1", "failedFBDeepLink")
                    editor.putString("sub2", "failedFBDeepLink")
                    editor.putString("sub3", "failedFBDeepLink")
                    editor.apply()
                } else {
                    editor.putString("sub1", appLinkData.targetUri!!.getQueryParameter("sub1"))
                    Log.d("deeplink", appLinkData.targetUri!!.getQueryParameter("sub1").toString())
                    editor.putString("sub2", appLinkData.targetUri!!.getQueryParameter("sub2"))
                    editor.putString("sub3", appLinkData.targetUri!!.getQueryParameter("sub3"))
                    editor.apply()
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    //getting data from adjust
    fun adjustDataGet(){
        // Configure adjust SDK.
        val environment = AdjustConfig.ENVIRONMENT_PRODUCTION
        val config = AdjustConfig(this, appToken, environment)

        // enable all logs
        config.setLogLevel(LogLevel.VERBOSE)
        Adjust.onCreate(config)
        Adjust.onResume()
        //attribution listener to get data
        config.setOnAttributionChangedListener { attribution ->
            trackerToken = attribution.trackerToken
            trackerName = attribution.trackerName
            network = attribution.network
            if (attribution.campaign == null || attribution.adgroup == null || attribution.creative == null ){
                campaign = "organic"
                adgroup = "organic"
                creative = "organic"
            } else {
                campaign = attribution.campaign
                adgroup = attribution.adgroup
                creative = attribution.creative
            }
            adid = attribution.adid

            //put to sharedprefs
            editor.putString("trackerToken", trackerToken)
            editor.putString("trackerName", trackerName)
            editor.putString("network", network)
            editor.putString("campaign", campaign)
            editor.putString("adgroup", adgroup)
            editor.putString("creative", creative)
            editor.putString("adid", adid)
            editor.commit()

            //getting deep link subid's
            if (settings.getString("network", "").toString() == "Facebook Installs") {
                //getting fb subid's
                getDeepLink()
            }


            //checking on first launch
            if (settings.getInt("firstlaunch", 0) == 1) {

            } else {
                //switch case
                if (settings.getString("network", "") == "Facebook Installs") {


                } else if (settings.getString("network", "") == "Google Ads UAC"){
                    editor.putString("sub1", "uac")
                    editor.putString("sub2", "uac")
                    editor.putString("sub3", "uac")

                } else if (settings.getString("network", "") == "Organic") {
                    editor.putString("sub1", "organic")
                    editor.putString("sub2", "organic")
                    editor.putString("sub3", "organic")


                } else if (settings.getString("network", "") == "Unattributed"){
                    editor.putString("sub1", "unattributed")
                    editor.putString("sub2", "unattributed")
                    editor.putString("sub3", "unattributed")

                }
            }

            //adding tag of first launch
            if (settings.getInt("firstlaunch", 0) == 1) {

            } else {
                editor.putInt("firstlaunch", 1)
            }

            editor.apply()

            //starting splash activity
            startActivity(Intent(this@Splash, BeforeMain::class.java))
        }

    }

}

