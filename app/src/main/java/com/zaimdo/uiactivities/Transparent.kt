package com.zaimdo.uiactivities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustConfig
import com.adjust.sdk.LogLevel
import com.onesignal.OneSignal
import com.zaimdo.R

class Transparent : Activity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transparent)
        startActivity(Intent(this@Transparent, Splash::class.java))
       /* //init sharedprefs
        settings = getSharedPreferences("LOCAL", Context.MODE_PRIVATE)
        editor = settings.edit()
        //getting font scale
        font = resources.configuration.fontScale




        //attribution listener to get data
        config.setOnAttributionChangedListener { attribution ->
            trackerToken = attribution.trackerToken
            Log.d("prefs", attribution.trackerToken)
            Log.d("prefs", attribution.trackerName)
            Log.d("prefs2", attribution.network)
            trackerName = attribution.trackerName
            network = attribution.network
            if (attribution.campaign == null || attribution.adgroup == null || attribution.creative == null ){
                campaign = "organic"
                adgroup = "organic"
                creative = "organic"
            } else {
                campaign = attribution.campaign
                adgroup = attribution.adgroup
                creative = attribution.creative
            }
            adid = attribution.adid

            //put to sharedprefs
            editor.putString("trackerToken", trackerToken)
            editor.putString("trackerName", trackerName)
            editor.putString("network", network)
            editor.putString("campaign", campaign)
            editor.putString("adgroup", adgroup)
            editor.putString("creative", creative)
            editor.putString("adid", adid)
            editor.apply()



            //calling method to get fb attribution
            //checking on first launch
            if (settings.getInt("firstlaunch", 0) == 1) {

            } else {
                //switch case
                if (settings.getString("network", "") == "Facebook Installs") {


                } else if (settings.getString("network", "") == "Google Ads UAC"){
                    editor.putString("sub1", "uac")
                    editor.putString("sub2", "uac")
                    editor.putString("sub3", "uac")

                } else if (settings.getString("network", "") == "Organic") {
                    editor.putString("sub1", "organic")
                    editor.putString("sub2", "organic")
                    editor.putString("sub3", "organic")


                } else if (settings.getString("network", "") == "Unattributed"){
                    editor.putString("sub1", "unattributed")
                    editor.putString("sub2", "unattributed")
                    editor.putString("sub3", "unattributed")

                }
            }

            //adding tag of first launch
            if (settings.getInt("firstlaunch", 0) == 1) {

            } else {
                editor.putInt("firstlaunch", 1)
            }

            editor.apply()


            //starting splash activity
            startActivity(Intent(this@Transparent, Splash::class.java))
        }
        Adjust.onCreate(config)
        Adjust.onResume()

        if (settings.getInt("firstlaunch", 0) == 1) {
            startActivity(Intent(this@Transparent, Splash::class.java))
        } else {

        }

    }
    //fb attribution subid method
    private fun getFBAtts() {
 */
    }
}