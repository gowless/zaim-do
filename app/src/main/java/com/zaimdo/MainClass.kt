package com.zaimdo

import android.app.Application

import android.content.SharedPreferences

import com.onesignal.OneSignal
import com.zaimdo.network.models.getmodels.Listoffers

class MainClass : Application() {
    companion object{
        val appToken = "vjw8r4kil2ww"
        private val ONESIGNAL_APP_ID = "eeed0a05-4d35-49ea-bc6c-999014c17a32"
        //declare main vars of information
        lateinit var trackerToken: String
        lateinit var trackerName: String
        lateinit var network: String
        lateinit var campaign: String
        lateinit var adgroup: String
        lateinit var creative: String
        lateinit var adid: String
        var font: Float = 0F
        val APPID = "com.zaimdo"
        val REGION = "ua"

        //declare sharedprefs
        lateinit var settings: SharedPreferences
        lateinit var editor: SharedPreferences.Editor

        //subid1
        lateinit var subid1: String

        //subid2
        lateinit var subid2: String

        //subid3
        lateinit var subid3: String

        //net
        lateinit var net: String

        //cam
        lateinit var cam: String

        //adg
        lateinit var adg: String

        //cre
        lateinit var cre: String

        //tabs strings
        lateinit var first: String
        lateinit var second: String
        lateinit var third: String
        lateinit var ad_id: String
        lateinit var carrier: String

        //number of tabs
        var numberOfTabs: Int = 0;

        //is empty field
        var isEmpty: Boolean = false

        //main data lists
        lateinit var listDataAll: List<Listoffers>
        lateinit var listDataBad: List<Listoffers>
        lateinit var listDataZero: List<Listoffers>

        var iteratedListZero: MutableList<Listoffers> = mutableListOf()
        var iteratedListBad: MutableList<Listoffers> = mutableListOf()
    }


    override fun onCreate() {
        super.onCreate()

        //getting font scale
        font = resources.configuration.fontScale

        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)

        // OneSignal Initialization
        OneSignal.initWithContext(this)
        OneSignal.setAppId(ONESIGNAL_APP_ID)

    }

}