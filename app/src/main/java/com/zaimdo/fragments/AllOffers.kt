package com.zaimdo.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zaimdo.MainClass.Companion.listDataAll
import com.zaimdo.adapters.BadListAdapter
import com.zaimdo.uiactivities.Splash
import com.zaimdo.R


class AllOffers : Fragment() {

    lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_all, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        val adapterBadList = BadListAdapter(listDataAll)
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapterBadList


        return view;
    }
}


