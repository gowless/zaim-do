package com.zaimdo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zaimdo.MainClass.Companion.iteratedListBad
import com.zaimdo.adapters.AllOffersAdapter
import com.zaimdo.uiactivities.Splash
import com.zaimdo.R


class BadList : Fragment() {

    lateinit var recyclerView: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_with_bad_list, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        val adapterAllMain = AllOffersAdapter(iteratedListBad)
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapterAllMain

        return view;
    }

    }
